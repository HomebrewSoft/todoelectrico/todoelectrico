# -*- coding: utf-8 -*-
from odoo import models, fields, api


class PurchaseOrderLines(models.Model):
    _inherit = "purchase.order.line"

    @api.onchange("product_id")
    def onchange_product_id(self):
        res = super(PurchaseOrderLines, self).onchange_product_id()
        self.name = self.with_context(
            display_default_code=False
        ).product_id.display_name
        return res
