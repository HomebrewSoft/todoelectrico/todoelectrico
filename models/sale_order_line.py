# -*- coding: utf-8 -*-
from odoo import models, fields, api


class SaleOrderLines(models.Model):
    _inherit = "sale.order.line"

    @api.onchange("product_id")
    def product_id_change(self):
        res = super(SaleOrderLines, self).product_id_change()
        self.name = self.with_context(
            display_default_code=False
        ).product_id.display_name
        return res
