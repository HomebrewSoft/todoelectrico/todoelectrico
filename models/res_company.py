# -*- coding: utf-8 -*-
from odoo import models, fields


class Company(models.Model):
    _inherit = 'res.company'

    whatsapp = fields.Char(
        string="Whatsapp number",
    )
