# -*- coding: utf-8 -*-
from odoo import models, fields


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    mapping_ids = fields.One2many(
        related='sale_id.mapping_ids'
    )
