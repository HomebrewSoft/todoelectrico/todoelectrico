# -*- coding: utf-8 -*-
from odoo import models, fields, api


class AccountInvoiceLines(models.Model):
    _inherit = "account.invoice.line"

    @api.onchange("product_id")
    def _onchange_product_id(self):
        res = super(AccountInvoiceLines, self)._onchange_product_id()
        self.name = self.with_context(
            display_default_code=False
        ).product_id.display_name
        return res
