# -*- coding: utf-8 -*-
from odoo import models, fields


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    mapping_ids = fields.One2many(
        comodel_name='wk.order.mapping',
        inverse_name='erp_order_id',
    )
