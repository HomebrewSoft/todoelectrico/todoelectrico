from odoo import api, fields, models


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    sale_id = fields.Many2one(
        comodel_name="sale.order",
        compute="_compute_sale_id",
    )

    def _compute_sale_id(self):
        for invoice in self:
            invoice.sale_id = self.env["sale.order"].search(
                [("name", "=", invoice.origin)], limit=1
            )
