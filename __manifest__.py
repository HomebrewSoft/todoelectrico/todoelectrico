# -*- coding: utf-8 -*-
{
    'name': 'TodoElectrico',
    'version': '12.0.1.10.1',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        "account_invoice_report_grouped_by_picking",
        "account",
        "delivery",
        "discount_monetary",
        "odoo_virtuemart_connector",
        "purchase_stock",
        "purchase",
        "sale_stock",
        "sale",
    ],
    "data": [
        # security
        # data
        # reports
        "reports/account_invoice.xml",
        "reports/purchase.xml",
        "reports/sale.xml",
        "reports/stock.xml",
        # views
        "views/res_company.xml",
    ],
}
